# Snowflake Generator & Animation in Pure CSS

A Pen created on CodePen.io. Original URL: [https://codepen.io/Baggypants/pen/mdjdqJJ](https://codepen.io/Baggypants/pen/mdjdqJJ).

Snowflake Generator & Animation in Pure CSS was created with only HTML elements, CSS3 pseudo classes and keyframes. It creates every snowflake randomly and then animates them randomly. 

It can have more snowflakes to create a snow storm by changing the number of flakes in the HAML and Sass. However, Codepen cannot render the Sass with high numbers of snowflakes.

Thanks to Emmanuel Pilande on help with the animation http://codepen.io/YoEmanYo/pen/owAnm?editors=110