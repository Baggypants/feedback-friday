init();
animate();
function init() {
  rad90 = Math.PI * 0.5;
  var WIDTH = window.innerWidth - 20,
    HEIGHT = window.innerHeight - 20;
  scene = new THREE.Scene();
//  scene.background = new THREE.Color( 0xff0000 );
  camera = new THREE.PerspectiveCamera(45, WIDTH / HEIGHT, 0.1, 10000);
  camera.position.z = 6;

  qatexture = new THREE.TextureLoader().load(
    "https://baggypants.gitlab.io/feedback-friday/images/beyondtrust-logo.png"
  );
  qalogo = new THREE.MeshBasicMaterial({ map: qatexture });
  qalogo.transparent = 1;
  //qalogo = new THREE.MeshBasicMaterial( {color: 'black'} );
  qacube = new THREE.BoxGeometry(2, 2, 2);
  qamesh = new THREE.Mesh(qacube, qalogo);
  //animate

  scene.add(qamesh);
  //Try TWEEN
  waitvar = 1000;
  //shiftit =

  spinit = new TWEEN.Tween(qamesh.rotation)
    .to({ z: qamesh.rotation.z + rad90 * 4 }, 2000)
    .delay(waitvar)
    .start();
  spinit2 = new TWEEN.Tween(qamesh.rotation)
    .to({ y: qamesh.rotation.y + rad90 }, 1000)
    .delay(waitvar);
  spinit3 = new TWEEN.Tween(qamesh.rotation)
    .to({ y: (qamesh.rotation.y = 0) }, 1000)
    .delay(waitvar);
  spinit4 = new TWEEN.Tween(qamesh.rotation)
    .to({ x: qamesh.rotation.x + rad90 }, 1000)
    .delay(waitvar);
  spinit5 = new TWEEN.Tween(qamesh.rotation)
    .to({ z: (qamesh.rotation.z = 0) }, 2000)
    .delay(waitvar);
  spinit6 = new TWEEN.Tween(qamesh.rotation).to(
    { y: qamesh.rotation.y + rad90 * 2, z: qamesh.rotation.z + rad90 * 2 },
    3000
  )
  .delay(waitvar);
  spinit7 = new TWEEN.Tween(qamesh.rotation).to(
    { y: qamesh.rotation.y + rad90 * 4, z: qamesh.rotation.z + rad90 * 4 },
    3000
  ).delay(waitvar);
  spinit8 = new TWEEN.Tween(qamesh.rotation).to(
    { y: 0, z: 0, x: 0 },
    3000
  ).delay(waitvar);
  shifit1 = new TWEEN.Tween(qamesh.position)
  .to({ z: qamesh.position.z + 8 }, 1000)
  .delay(waitvar);
  shifit2 = new TWEEN.Tween(qamesh.position)
  .to({ z: qamesh.position.z = 0 }, 1000);
  
    shifit3 = new TWEEN.Tween(qamesh.position)
  .to({ z: qamesh.position.z - 16 }, 1000)
  .delay(waitvar);
  shifit4 = new TWEEN.Tween(qamesh.position)
  .to({ z: qamesh.position.z = 0 }, 1000);
  
  batspin = new TWEEN.Tween(qamesh.rotation)
    .to({ z: qamesh.rotation.z + (rad90 * 8) }, 1000)
  .delay(waitvar);
  batspinbk = new TWEEN.Tween(qamesh.rotation)
    .to({ z: 0 }, 1000);
  
  
  spinit.chain(spinit2);
  spinit2.chain(spinit3);
  spinit3.chain(spinit4);
  spinit4.chain(spinit5);
  spinit5.chain(spinit6);
  spinit6.chain(spinit7);
  spinit7.chain(spinit8);
  spinit8.chain(shifit1);
  shifit1.chain(shifit2);
  shifit2.chain(shifit3, batspin);
  shifit3.chain(shifit4,batspinbk)
  shifit4.chain(spinit);
  

  renderer = new THREE.WebGLRenderer({ alpha: true });
  renderer.setSize(WIDTH, HEIGHT);
  window.addEventListener("resize", function () {
    var WIDTH = window.innerWidth - 20,
      HEIGHT = window.innerHeight - 20;
    renderer.setSize(WIDTH, HEIGHT);
    camera.aspect = WIDTH / HEIGHT;
    camera.updateProjectionMatrix();
  });

  //render
  document.body.appendChild(renderer.domElement);
}

function animate() {
  requestAnimationFrame(animate);
  TWEEN.update();
  //  createjs.RWEEN.update();
  renderer.render(scene, camera);
}

