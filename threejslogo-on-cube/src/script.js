init();
animate();
function init() {
  rad90 = Math.PI * 0.5;
  var WIDTH = window.innerWidth - 20,
    HEIGHT = window.innerHeight - 20;
  scene = new THREE.Scene();
  scene.background = new THREE.Color("limegreen");
  camera = new THREE.PerspectiveCamera(45, WIDTH / HEIGHT, 0.1, 10000);
  camera.position.z = 6;

  qatexture = new THREE.TextureLoader().load(
    "https://baggypants.gitlab.io/feedback-friday/images/qa-large.png"
  );
  qalogo = new THREE.MeshBasicMaterial({ map: qatexture });
  qalogo.transparent = 1;
  //qalogo = new THREE.MeshBasicMaterial( {color: 'black'} );
  qacube = new THREE.BoxGeometry(2, 2, 2);
  qamesh = new THREE.Mesh(qacube, qalogo);
  //animate

  scene.add(qamesh);
  //Try TWEEN
  waitvar = 60000
  spinit = new createjs.Tween(qamesh.rotation, { loop: true } )
    .to({ y: qamesh.rotation.y + rad90 }, 1000).wait(waitvar)
    .to({ y: qamesh.rotation.y = 0 }, 1000).wait(waitvar)
    .to({ z: qamesh.rotation.z + rad90 *4}, 2000).wait(waitvar)
    .to({ y: qamesh.rotation.y + rad90 *2, z: qamesh.rotation.z + rad90 * 2}, 3000)
    .to({ y: qamesh.rotation.y + rad90 *4, z: qamesh.rotation.z + rad90 * 4}, 3000).wait(waitvar);
  //timeit = new createjs.Timeline().addTween(spinit);
  //timeit.loop = -1;
  //spinit.easing( createjs.TWEEN.Easing.Quadratic.EaseOut);
  
  createjs.Ticker.timingMode = createjs.Ticker.RAF;
  createjs.Ticker.addEventListener("tick", animate);
  
  
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(WIDTH, HEIGHT);
  window.addEventListener("resize", function () {
    var WIDTH = window.innerWidth - 20,
      HEIGHT = window.innerHeight - 20;
    renderer.setSize(WIDTH, HEIGHT);
    camera.aspect = WIDTH / HEIGHT;
    camera.updateProjectionMatrix();
  });

  //render
  document.body.appendChild(renderer.domElement);
}

function animate() {
  requestAnimationFrame(animate);

  //  createjs.RWEEN.update();
  renderer.render(scene, camera);
}
