const zoneList = document.getElementById('zoneList')
function calibrate(zone) {
  const date = new Date()
  const option = { year: "numeric", month: "short", day: "2-digit", hour: "2-digit", minute: "2-digit", hour12: false }//設定 toLocaleString 回傳資料
  Object.assign(option, { timeZone: zone })//傳入指定時區
  let timeStr = date.toLocaleString('en', option).split(',')//拆解資料
  let dateStr = timeStr[0].split(' ').reverse()//日 月 翻轉
    timeStr.shift()//移除舊日期資料
    timeStr.map((d, i) => { timeStr[i] = timeStr[i].trim() })//去除空白
    dateStr =[...dateStr,...timeStr]//合併日期 時間
  return dateStr
}

const timeZoneArray = ['America/Phoenix', 'America/Halifax', 'Europe/London', 'Asia/Singapore']
setTimeout(function () {
  let str = ''
  for (const index in timeZoneArray) {
    let calibrateTime = calibrate(timeZoneArray[index])//傳入陣列時區
    str += `<li class="${ calibrateTime[3].split(':')[0]>11?'night':'day'}">
              <div class="zone">
                <h2>${timeZoneArray[index].split('/')[1].replace('_',' ')}</h2>
                <span>${calibrateTime[0]+' '+calibrateTime[1]+'. '+calibrateTime[2]}</span>
              </div>
              <div class="time">${calibrateTime[3]}</div>
            </li>`
  }
  zoneList.innerHTML = str
  setTimeout(arguments.callee, 60000)
})