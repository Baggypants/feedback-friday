# [webkit] Animated "text-shadow" pattern

A Pen created on CodePen.io. Original URL: [https://codepen.io/Baggypants/pen/OJxyJqv](https://codepen.io/Baggypants/pen/OJxyJqv).

Uses `-webkit-background-clip: text` & `linear-gradient` to simulate striped text shadow.
