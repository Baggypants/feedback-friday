# Opening Credits

A Pen created on CodePen.io. Original URL: [https://codepen.io/Baggypants/pen/OJQjoBd](https://codepen.io/Baggypants/pen/OJQjoBd).

Text animated with CSS to recreate the opening titles of a movie.