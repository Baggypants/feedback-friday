# Thumbs Up!

A Pen created on CodePen.io. Original URL: [https://codepen.io/Baggypants/pen/oNGVQWw](https://codepen.io/Baggypants/pen/oNGVQWw).

Thumbs up animation designed by https://dribbble.com/daryl. Still need improvement on the code for the dot animation.
